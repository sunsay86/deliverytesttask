
import Foundation

struct Location: Codable {

    let street: String
    let city: String
    let state: String
    let country: String
    let timezone: String

    static func emptyInit() -> Location {
        return Location(
            street: "",
            city: "",
            state: "",
            country: "",
            timezone: ""
        )
    }
}
