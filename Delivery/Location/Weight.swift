
import Foundation

struct Weight: Identifiable {
    
    var id = UUID()
    var position: Int
    var baseCost: Int
    var weight: String
    
}
