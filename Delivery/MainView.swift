
import SwiftUI

struct MainView: View {
    
    @ObservedObject var mainViewModel = MainViewModel()

    var body: some View {
        NavigationView {
            
            VStack {

//                if mainViewModel.stateView  == .success {
                CurrentUserInfo(data: mainViewModel.currentUser, dataDeliveryTo: mainViewModel.deliveryToUser, withURL: mainViewModel.currentUser.picture)
                Spacer()
            }
         
            .navigationBarHidden(true)
            .navigationBarTitle(Text(""))
        }
    }

}



struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
